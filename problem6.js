/* ==== Problem #6 ====
 A buyer is interested in seeing only BMW and Audi cars within the inventory. 
 Execute a function and return an array that only contains BMW and Audi cars. 
  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
*/

//Taking the query, searching in the inventory and returning the found result
function problem6(inventory, ...query) {
	if (typeof inventory === "undefined") {
		return [];
	} else if (inventory.length === 0) {
		return [];
	} else {
		//Removing the duplicate queries
		query = [...new Set(query)];

		let result = [];
		if (query.length === 0) return result;
		else {
			for (
				let inventoryIndex = 0;
				inventoryIndex < inventory.length;
				inventoryIndex++
			) {
				for (let queryIndex = 0; queryIndex < query.length; queryIndex++) {
					if (inventory[inventoryIndex].car_make === query[queryIndex]) {
						result.push(inventory[inventoryIndex]);
					}
				}
			}
		}
		return JSON.stringify(result);
	}
}
module.exports = problem6;
