/* ==== Problem #5 ====
The car lot manager needs to find out how many cars are older than the year 2000.
 Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and
 return the array of older cars and log its length.
*/

//Filtering out the years< 2000 and returing the resultant array
function problem5(allYears) {
	if (typeof allYears === "undefined") {
		return [];
	} else if (allYears.length === 0) {
		return [];
	} else {
		let oldCarYears = [];
		for (let index = 0; index < allYears.length; index++) {
			if (allYears[index] < 2000) {
				oldCarYears.push(allYears[index]);
			}
		}
		return oldCarYears.length;
	}
}
module.exports = problem5;
