/* ==== Problem #3 ====
 The marketing team wants the car models listed alphabetically on the website.
  Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
*/

//Sorting the inventory in ascending order of car_modle names using a comparator.
function problem3(inventory) {
	if (typeof inventory === "undefined") {
		return [];
	} else if (inventory.length === 0) {
		return [];
	} else {
		inventory.sort((a, b) => {
			//Comparator to sort alphabetically
			let first = a.car_model.toLowerCase();
			let second = b.car_model.toLowerCase();
			if (first < second) return -1;
			else if (first > second) return 1;
			else return 0;
		});
		return inventory;
	}
}
module.exports = problem3;
