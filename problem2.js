/* ==== Problem #2 ====
The dealer needs the information on the last car in their inventory. 
Execute a function to find what the make and model of the last car in the inventory is?  
Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*";
*/

//Finds the last Index of inventory and return it
function problem2(inventory) {
	let lastIndex;
	if (typeof inventory === "undefined") {
		return [];
	} else if (inventory.length === 0) {
		return [];
	} else {
		lastIndex = inventory.length - 1;
	}
	return (
		"Last Car is a " +
		inventory[lastIndex].car_make +
		" " +
		inventory[lastIndex].car_model
	);
}
module.exports = problem2;
