/* ==== Problem #1 ====
 The dealer can't recall the information for a car with an id of 33 on his lot.
 Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
 Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
*/

//Finds the inventory with particular id and returns string having details.
function problem1(inventory, id) {
	//checks if id or inventory is undefined or not.
	if (typeof id === "undefined" || typeof inventory === "undefined") return [];
	else {
		//iterates through the inventory to find the matching id.
		for (let index = 0; index < inventory.length; index++) {
			if (inventory[index].id === id) {
				let dataObj = inventory[index];
				return dataObj;
			}
		}
	}
	return [];
}

module.exports = problem1;
